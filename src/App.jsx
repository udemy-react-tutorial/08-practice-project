import {Header} from "./components/Header.jsx";
import {Result} from "./components/Result.jsx";
import {UserInput} from "./components/UserInput.jsx";
import {useState} from "react";
import {updateResult} from "./util/update-result.js";

const INITIAL_INPUTS = {
    initialInvestment: 1000,
    annualInvestment: 1200,
    expectedReturn: 6,
    duration: 10
}

function areInputsValid(userInputs) {
    return userInputs.duration >= 1
        && userInputs.expectedReturn >= 1
        && userInputs.initialInvestment >= 1
        && userInputs.annualInvestment >= 1;
}

function App() {
    const [userInputs, setUserInputs] = useState(INITIAL_INPUTS);
    const validInput = areInputsValid(userInputs)

    function handleInputUpdate(event) {
        setUserInputs((prevUserInputs) => {
            return updateResult(event, prevUserInputs);
        });
    }

    return (
        <>
            <Header/>
            <UserInput userInputs={userInputs} onUserInputChange={handleInputUpdate}/>
            {! validInput && <p className="center">Inputs are invalid! Please input proper values</p>}
            {validInput && <Result userInputs={userInputs}/>}
        </>
    )
}

export default App
