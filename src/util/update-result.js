export function updateResult(event, prevState) {
    const updatedResults = {...prevState};
    switch (event.target.id) {
        case 'initial-investment':
            updatedResults.initialInvestment = parseInt(event.target.value);
            break;
        case 'annual-investment':
            updatedResults.annualInvestment = parseInt(event.target.value);
            break;
        case 'expected-return':
            updatedResults.expectedReturn = parseInt(event.target.value);
            break;
        case 'duration':
            updatedResults.duration = parseInt(event.target.value);
            break;
        default:
            console.log("Invalid target id!")
    }
    return updatedResults;
}