export function UserInput({userInputs, onUserInputChange}) {
    return (
        <section id="user-input">
            <form onChange={(event) => onUserInputChange(event)}>
                <div className="input-group">
                    <p>
                        <label htmlFor="initial-investment">Initial investment</label>
                        <input type="number" id="initial-investment" required defaultValue={userInputs.initialInvestment}/>
                    </p>
                    <p>
                        <label htmlFor="annual-investment">Annual investment</label>
                        <input type="number" id="annual-investment" required defaultValue={userInputs.annualInvestment}/>
                    </p>
                </div>
                <div className="input-group">
                    <p>
                        <label htmlFor="expected-return">Expected return</label>
                        <input type="number" id="expected-return" required defaultValue={userInputs.expectedReturn}/>
                    </p>
                    <p>
                        <label htmlFor="duration">Duration</label>
                        <input type="number" id="duration" required defaultValue={userInputs.duration}/>
                    </p>
                </div>
            </form>
        </section>
    )
}